package com.xsc;

public class Jumper {
    // https://leetcode-cn.com/problems/jump-game/solution/chao-xiang-xi-tu-jie-di-gui-tan-xin-55-tiao-yue-yo/
    public boolean canJump(int[] nums) {
        boolean[] dest = new boolean[nums.length];
        dest[0] = true;
        for (int i = 0; i < nums.length; i++) {
            if (!dest[i]) {
                continue;
            }
            for (int j = 1; j <= nums[i] && i + j < nums.length; j++) {
                dest[i + j] = true;
                if (dest[nums.length - 1]) {
                    return true;
                }
            }
        }
        return dest[nums.length - 1];
    }
}
