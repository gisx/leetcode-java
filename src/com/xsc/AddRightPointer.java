package com.xsc;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class AddRightPointer {
    public Node connect(Node root) {
        if (root == null) return null;
        Deque<Node> deque = new ArrayDeque<>();
        deque.add(root);
        while (!deque.isEmpty()) {
            Node pre =null;
            int size = deque.size();
            for (int i = 0; i < size; i++) {
                Node cur = deque.pollFirst();
                if (cur.left != null) {
                    deque.add(cur.left);
                }
                if (cur.right != null) {
                    deque.add(cur.right);
                }

                if (pre != null) {
                    pre.next = cur;
                }
                pre = cur;
            }
        }
        return root;
    }
}

class Node {
    public int val;
    public Node left;
    public Node right;
    public Node next;

    public Node() {
    }

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, Node _left, Node _right, Node _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
};
