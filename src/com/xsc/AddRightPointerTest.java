package com.xsc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddRightPointerTest {

    AddRightPointer app;
    @Before
    public void setUp() throws Exception {
        app = new AddRightPointer();
    }

    @Test
    public void connect() {
        Node node = new Node(1);
        node.left = new Node(2);
        node.right = new Node(3);
        node.left.left = new Node(4);
        node.left.right = new Node(5);
        node.right.left = new Node(6);
        node.right.right = new Node(7);

        app.connect(node);
    }
}