package com.xsc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PhoneNumTest {

    PhoneNum app;

    @Before
    public void setUp() throws Exception {
        app = new PhoneNum();
    }

    @Test
    public void letterCombinations() {
        assertEquals(9, app.letterCombinations("23").size());
    }
}