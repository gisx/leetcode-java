package com.xsc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IslandCountTest {
    IslandCount app;

    @Before
    public void setUp() throws Exception {
        app = new IslandCount();
    }

    @Test
    public void numIslands() {
        assertEquals(1, app.numIslands(new char[][]{
                {'1', '1', '1', '1', '0'},
                {'1', '1', '0', '1', '0'},
                {'1', '1', '0', '0', '0'},
                {'0', '0', '0', '0', '0'}
        }));

    }

    @Test
    public void numsIslands2() {
        assertEquals(3, app.numIslands(new char[][]{
                        {'1', '1', '0', '0', '0'},
                        {'1', '1', '0', '0', '0'},
                        {'0', '0', '1', '0', '0'},
                        {'0', '0', '0', '1', '1'}
                }
        ));
    }
}