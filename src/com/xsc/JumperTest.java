package com.xsc;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class JumperTest {

    Jumper app;

    @Before
    public void setUp() throws Exception {
        app = new Jumper();
    }

    @Test
    public void canJump() {
        assertTrue(app.canJump(new int[]{2,3,1,1,4}));
    }

    @Test
    public void canJump2() {
        assertFalse(app.canJump(new int[]{3,2,1,0,4}));
    }
}