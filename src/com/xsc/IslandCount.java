package com.xsc;

import java.awt.*;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;

public class IslandCount {
    public int numIslands(char[][] grid) {
        int cnt = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == '1') {
                    cnt++;
                    dfs(grid, i, j);
                }
            }
        }
        return cnt;
    }


    private void dfs(char[][] grid, int i, int j) {
        Deque<Integer> poss = new ArrayDeque<>();
        poss.push(i << 16 | j);
        while (!poss.isEmpty()) {
            Integer pos = poss.pop();
            int x = pos >> 16;
            int y = pos << 16 >> 16;
            if (isInArea(grid, x + 1, y) && grid[x + 1][y] == '1') {
                grid[x + 1][y] = '0';
                poss.push((x + 1) << 16 | y);
            }
            if (isInArea(grid, x - 1, y) && grid[x - 1][y] == '1') {
                grid[x - 1][y] = '0';
                poss.push((x - 1) << 16 | y);
            }
            if (isInArea(grid, x, y + 1) && grid[x][y + 1] == '1') {
                grid[x][y + 1] = '0';
                poss.push(x << 16 | (y + 1));
            }
            if (isInArea(grid, x, y - 1) && grid[x][y - 1] == '1') {
                grid[x][y - 1] = '0';
                poss.push(x << 16 | (y - 1));
            }
        }
    }

    private void bfs(char[][] grid, int i, int j) {
        Deque<Integer> poss = new ArrayDeque<>();
        poss.push(i << 16 | j);
        while (!poss.isEmpty()) {
            Integer pos = poss.poll();
            int x = pos >> 16;
            int y = pos << 16 >> 16;
            if (isInArea(grid, x + 1, y) && grid[x + 1][y] == '1') {
                grid[x + 1][y] = '0';
                poss.push((x + 1) << 16 | y);
            }
            if (isInArea(grid, x - 1, y) && grid[x - 1][y] == '1') {
                grid[x - 1][y] = '0';
                poss.push((x - 1) << 16 | y);
            }
            if (isInArea(grid, x, y + 1) && grid[x][y + 1] == '1') {
                grid[x][y + 1] = '0';
                poss.push(x << 16 | (y + 1));
            }
            if (isInArea(grid, x, y - 1) && grid[x][y - 1] == '1') {
                grid[x][y - 1] = '0';
                poss.push(x << 16 | (y - 1));
            }
        }
    }

    private boolean isInArea(char[][] grid, int i, int j) {
        return i >= 0 && i < grid.length && j >= 0 && j < grid[0].length;
    }
}
