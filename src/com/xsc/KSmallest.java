package com.xsc;

import java.util.ArrayDeque;
import java.util.Deque;

public class KSmallest {
    public int kthSmallest(TreeNode root, int k) {
        Deque<TreeNode> deque = new ArrayDeque<>();
        TreeNode p = root;

        int n = 0;
        while (!deque.isEmpty() || p != null) {
            if (p != null) {
                deque.push(p);
                p = p.left;
            } else {
                p = deque.pop();
                n++;
                if (n == k) return p.val;
                p = p.right;
            }
        }
        return 0;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}